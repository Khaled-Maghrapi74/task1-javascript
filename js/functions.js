/*
function factorial(n){
    for(let i = n ; i>=1 ; i--){
        n = n * i ;
    }
    return n;
}
console.log(factorial(4));
*/
function factorial(n) {
    return (n != 1) ? n * factorial(n - 1) : 1;
}
console.log(factorial(4)); 